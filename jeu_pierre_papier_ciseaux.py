
from random import randint

#Déclaration des variables
#Les élements du jeu 
t = ["Pierre", "Papier", "Ciseaux"] #Selectionner parmi ces options 

 
comp_choice = t[randint(0,2)] #Permet de faire une selection aléatoire 
player_choice = 1
computer_win=0
player_win=0
#C'est parti 
print("****************BIENVENUE DANS LE JEU PIERRE, PAPIER, CISEAUX********************")
print("****************Master SIEE ------ 2021/2022--------PARIS DAUPHINE**********")
print("Les règles de victoire du jeu Pierre, Papier, et Ciseaux sont les suivantes :")
print("Pierre contre papier = papier gagne")
print("Pierre contre ciseaux = Pierre gagne")
print("papier contre ciseaux = ciseaux gagne")
print("------------------------------------------------------------------------------")
print("------------------------------------------------------------------------------")
print("GO!!!!!!!!!!!!!!!!!!!!!!!!!!!")
print("/")
print("/")
#GO GO 

#Choix de l'option par le joueur
while player_choice == 1:
    player_choice = input("Choisissez entre Pierre, Papier ou Ciseaux ?")
    comp_choice = t[randint(0,2)]
    if player_choice == comp_choice:
        print("Egalité !") #Affiche si égalité de score 
        print("Score")
        print("L'ordinateur a gagné:",computer_win)
        print("Le joueur a gagné:",player_win)
    elif player_choice == "Pierre":
        if comp_choice == "Papier":
            print("Vous avez perdu ! L'ordinateur a choisi", comp_choice, "! Le joueur a choisi", player_choice)
            computer_win+=1 # Condition permettant d'afficher si l'utilisateur a perdu
            print("Score")
            print("L'ordinateur a gagné:",computer_win)
            print("Le joueur a gagné:",player_win)
             
        else:
            print("Vous avez gagné ! Le joueur a choisi", player_choice, "! L'ordinateur a choisi", comp_choice)
            player_win+=1   #Condition permettant d'afficher si l'utilisateur a gagné 
            print("Score")
            print("L'ordinateur a gagné:",computer_win)
            print("Le joueur a gagné:",player_win)
    elif player_choice == "Papier":
        if comp_choice == "Ciseaux":
            print("Vous avez perdu ! L'ordinateur a choisi", comp_choice, "! Le joueur a choisi", player_choice)
            computer_win+=1
            print("Score")
            print("L'ordinateur a gagné:",computer_win)
            print("Le joueur a gagné:",player_win)
        else:
            print("Vous avez gagné ! Le joueur a choisi", player_choice, "! L'ordinateur a choisi", comp_choice)
            player_win+=1
            print("Score")
            print("L'ordinateur a gagné:",computer_win)
            print("Le joueur a gagné:",player_win)
    elif player_choice == "Ciseaux":
        if comp_choice == "Pierre":
            print("Vous avez perdu ! L'ordinateur a choisi", comp_choice, "! Le joueur a choisi", player_choice)
            computer_win+=1
            print("Score")
            print("L'ordinateur a gagné:",computer_win)
            print("Le joueur a gagné:",player_win)
        else:
            print("Vous avez gagné ! Le joueur a choisi", player_choice, "! L'ordinateur a choisi", comp_choice)
            player_win+=1
            print("Score")
            print("L'ordinateur a gagné:",computer_win)
            print("Le joueur a gagné:",player_win)
    else:
        print("Entrer invalide!") #Condition si entrer invalide 
        
    #Pour continuer le jeu dans la boucle
    #Dire oui ou non si vous voulez continuer
        #Oui pour continuer et non pour arreter le programme
    print("")
    ch=input("Voulez-vous continuer?(oui/non)") 
    if ch=="oui":
        player_choice=1 
        
    else:
        break
 
